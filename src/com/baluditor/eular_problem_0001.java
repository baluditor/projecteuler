package com.baluditor;

public class eular_problem_0001 {

    public static void main(String args[]) {

        int sum = 0;

        for (int i=0; i<1000; i++){
            if (i%3==0 || i%5==0){
                sum+=i;
            }
        }
        System.out.println(sum); //<- check the sum to submit
    }

}
